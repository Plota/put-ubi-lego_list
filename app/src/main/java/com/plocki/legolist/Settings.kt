package com.plocki.legolist

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import kotlinx.android.synthetic.main.activity_settings.*



class Settings : AppCompatActivity() {

    var extension = "xml"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        radioGroup.setOnCheckedChangeListener { radioGroup, i ->
            when(radioGroup.checkedRadioButtonId){
                radioXml.id ->{
                    extension = "xml"
                }
                radioCsv.id ->{
                    extension = "csv"
                }
            }
        }
    }

    fun end(v: View){
        val returnIntent = Intent()
        returnIntent.putExtra("extension", extension)
        returnIntent.putExtra("prefix",prefix.text.toString())
        setResult(Activity.RESULT_OK, returnIntent)
        finish()
    }
}
