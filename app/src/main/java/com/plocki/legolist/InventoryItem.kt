package com.plocki.legolist

import android.database.sqlite.SQLiteDatabase

class InventoryItem {
    var id: Int
    var inventoryId: Int
    var typeId: Int
    var itemId: Int
    var QuantityInStore: Int
    var QuantityInSet: Int
    var ColorId: Int
    var Extra: Int

    constructor(str: String) {
        var values = str.split(",")
        this.id = values[0].toInt()
        this.inventoryId = values[1].toInt()
        this.typeId = values[2].toInt()
        this.itemId = values[3].toInt()
        this.QuantityInStore = values[4].toInt()
        this.QuantityInSet = values[5].toInt()
        this.ColorId = values[6].toInt()
        this.Extra = values[7].toInt()
    }

    fun getColorName(db: SQLiteDatabase): String {
        val typetmp = this.ColorId
        var c_type = db.rawQuery("SELECT * FROM COLORS WHERE ID = '$typetmp';", null)
        c_type.moveToNext()
        var type = c_type.getString(2)
        return type
    }

    fun getColorCode(db: SQLiteDatabase): String {
        val typetmp = this.ColorId
        var c_type = db.rawQuery("SELECT * FROM COLORS WHERE ID = '$typetmp';", null)
        c_type.moveToNext()
        var type = c_type.getInt(1)
        return type.toString()
    }

    fun getItemName(db: SQLiteDatabase): String {
        val typetmp = this.itemId
        var c_type = db.rawQuery("SELECT * FROM PARTS WHERE id = '$typetmp';", null)
        c_type.moveToNext()
        var type = c_type.getString(3)
        return type
    }

    fun getItemCode(db: SQLiteDatabase): String {
        val typetmp = this.itemId
        var c_type = db.rawQuery("SELECT * FROM PARTS WHERE id = '$typetmp';", null)
        c_type.moveToNext()
        var type = c_type.getString(2)
        return type
    }

    fun getTypeCode(db: SQLiteDatabase): String {
        val typetmp = this.typeId
        var c_type = db.rawQuery("SELECT * FROM ITEMTYPES WHERE id = '$typetmp';", null)
        c_type.moveToNext()
        var type = c_type.getString(1)
        return type
    }
}

