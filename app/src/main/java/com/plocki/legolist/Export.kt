package com.plocki.legolist

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.support.v4.app.ActivityCompat
import android.support.v4.content.FileProvider
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_export.*
import java.io.File
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.transform.OutputKeys
import javax.xml.transform.TransformerFactory
import javax.xml.transform.dom.DOMSource
import javax.xml.transform.stream.StreamResult


class Export : AppCompatActivity() {
    var fileName = ""
    var inventoryId = 0
    lateinit var  databaseAcces: DatabaseAcces
    val list = arrayListOf<InventoryItem>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_export)

        inventoryId = intent.getStringExtra("inventoryId").toInt()
        databaseAcces = DatabaseAcces(applicationContext)
        databaseAcces.open()

        var objects = databaseAcces.listParts(inventoryId)
        for (obj in objects) {
            val tmp = InventoryItem(obj)
            list.add(tmp)
        }
        radioOption.setOnCheckedChangeListener { radioGroup, i ->
            when(radioOption.checkedRadioButtonId){
                radioOnline.id -> {plainMail.visibility = View.INVISIBLE}
                radioEmail.id ->{plainMail.visibility = View.VISIBLE}
                radioSD.id -> {plainMail.visibility = View.INVISIBLE}
            }
        }
    }


    private fun prepareXml(){
        try{
            val docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder()
            val doc = docBuilder.newDocument()

            val rootElement = doc.createElement("INVENTORY")
            for(invitem in list) {
                val tmp = invitem.QuantityInStore - invitem.QuantityInSet
                if (tmp != 0) {

                    val item = doc.createElement("ITEM")

                    val itemtype = doc.createElement("ITEMTYPE")
                    itemtype.appendChild(doc.createTextNode(invitem.getTypeCode(databaseAcces.getDB())))
                    item.appendChild(itemtype)
                    val itemID = doc.createElement("ITEMID")
                    itemID.appendChild(doc.createTextNode(invitem.getItemCode(databaseAcces.getDB())))
                    item.appendChild(itemID)
                    val color = doc.createElement("COLOR")
                    color.appendChild(doc.createTextNode(invitem.getColorCode(databaseAcces.getDB())))
                    item.appendChild(color)
                    val qtyfilled = doc.createElement("QTYFILLED")
                    qtyfilled.appendChild(doc.createTextNode(tmp.toString()))
                    item.appendChild(qtyfilled)

                    rootElement.appendChild(item)
                }
            }
            doc.appendChild(rootElement)
            val transformer = TransformerFactory.newInstance().newTransformer()

            transformer.setOutputProperty(OutputKeys.INDENT,"yes")
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amout","2")

            val root = Environment.getExternalStorageDirectory().toString()
            val myDir = File("$root/lego")
            if (!myDir.exists()) {
                myDir.mkdirs()
            }

            val file = File(myDir,"$fileName.xml")
            if(isStoragePermissionGranted()){
                transformer.transform(DOMSource(doc),StreamResult(file))
            }


        }catch (e : Exception){
            Toast.makeText(this,e.toString(),Toast.LENGTH_SHORT).show()
        }

    }

    fun isStoragePermissionGranted(): Boolean {
        val TAG = "Storage Permission"
        if (Build.VERSION.SDK_INT >= 23) {
            if (this.checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission is granted")
                return true
            } else {
                Log.v(TAG, "Permission is revoked")
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), 1)
                return false
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG, "Permission is granted")
            return true
        }
    }

    fun isStoragePermissionGrantedRead(): Boolean {
        val TAG = "Storage Permission"
        if (Build.VERSION.SDK_INT >= 23) {
            if (this.checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission2 is granted")
                return true
            } else {
                Log.v(TAG, "Permission2 is revoked")
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), 1)
                return false
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG, "Permission2 is granted")
            return true
        }
    }

    public fun cancel(v : View){
        finish()
    }

    public fun send(v: View){
        fileName = plainName.text.toString()

        when(radioOption.checkedRadioButtonId) {
            radioEmail.id -> {
                prepareXml()
                if(isStoragePermissionGrantedRead()){
                    val root = Environment.getExternalStorageDirectory().absolutePath.toString()
                    val myDir = File("$root/lego")
                    val filelocation = File(myDir,"$fileName.xml")
                    val fileUri = FileProvider.getUriForFile(this,"com.myfileprovider",filelocation)

                    val emailIntent = Intent(Intent.ACTION_SEND)
                    // set the type to 'email'
                    emailIntent.type = "vnd.android.cursor.dir/email"
                    val to = arrayOf(plainMail.text.toString())
                    emailIntent.putExtra(Intent.EXTRA_EMAIL, to)
                    // the attachment
                    emailIntent.putExtra(Intent.EXTRA_STREAM, fileUri)
                    // the mail subject
                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject")
                    startActivity(Intent.createChooser(emailIntent, "Send email..."))
                }
                finish()
            }
            radioOnline.id -> {
                Toast.makeText(this,"TODO",Toast.LENGTH_SHORT).show()
            }
            radioSD.id ->{
                prepareXml()
                finish()
            }
            else ->{
                Toast.makeText(this,"Nie wybrano opcji",Toast.LENGTH_SHORT).show()
            }
        }

    }
}

