package com.plocki.legolist

import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.os.StrictMode
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.EditText
import kotlinx.android.synthetic.main.activity_main.*
import org.w3c.dom.Element
import org.w3c.dom.Node
import org.w3c.dom.NodeList
import org.xml.sax.InputSource
import java.net.URL
import javax.xml.parsers.DocumentBuilderFactory


class MainActivity : AppCompatActivity() {

    var num = ""
    var proj_name = ""
    var itemListed = ArrayList<Item>()
    lateinit var  databaseAcces: DatabaseAcces
    val EXTENSION_TAG = 1
    var extension = "xml"
    var prefix = "http://fcds.cs.put.poznan.pl/MyWeb/BL/"

    override fun onResume() {
        super.onResume()
        var list = databaseAcces.listProjects()
        val arrayAdapter = ArrayAdapter(this,android.R.layout.simple_list_item_1,list)
        listProjects.adapter = arrayAdapter
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var db = DatabaseHelper(this)
        if (android.os.Build.VERSION.SDK_INT > 9) {
            val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
            StrictMode.setThreadPolicy(policy)
        }
        databaseAcces = DatabaseAcces(applicationContext)
        databaseAcces.open()

        var list = databaseAcces.listProjects()
        val arrayAdapter = ArrayAdapter(this,android.R.layout.simple_list_item_1,list)
        listProjects.adapter = arrayAdapter
        listProjects.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
            val selectedItem = parent.getItemAtPosition(position) as String
            val tmp = selectedItem.split(":")
            val intent = Intent(this, ListOfBricks::class.java)
            intent.putExtra("inventoryId", tmp.get(0))
            startActivity(intent)

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            extension = data!!.getStringExtra("extension")
            prefix = data!!.getStringExtra("prefix")
        }
    }

    fun addProject(){
        if(itemListed.isNotEmpty()){
            val tmp = databaseAcces.addProject(proj_name,itemListed)
            val intent = Intent(applicationContext, ListOfBricks::class.java)
            intent.putExtra("inventoryId",tmp.toString())
            startActivity(intent)
        }
    }

    private fun download(){
        val urlString = prefix + num + ".$extension"
        var items = ArrayList<Item>()
        try {
            var url = URL(urlString)
            var dbf = DocumentBuilderFactory.newInstance()
            var db = dbf.newDocumentBuilder()
            var doc = db.parse(InputSource(url.openStream()))
            doc.documentElement.normalize()

            var itemList: NodeList = doc.getElementsByTagName("ITEM")

            for(i in 0 until itemList.length)
            {
                var bookNode: Node = itemList.item(i)

                if (bookNode.getNodeType() === Node.ELEMENT_NODE) {
                    val elem = bookNode as Element
                    val childNodes = elem.childNodes
                    var itemtype = childNodes.item(1).firstChild.nodeValue
                    var id =  childNodes.item(3).firstChild.nodeValue
                    var qis = childNodes.item(5).firstChild.nodeValue
                    var color = childNodes.item(7).firstChild.nodeValue
                    var extra = childNodes.item(9).firstChild.nodeValue
                    val item = Item(itemtype,id,qis,color,extra)
                    items.add(item)

                }
            }
            itemListed = items
            addProject()
        }catch (er: Exception){
            Log.e("ERR",er.toString())
        }
    }

    fun downloadCsv(){

    }

    fun showAddItemDialog(v: View){
        val alert = AlertDialog.Builder(this)
        val edittext = EditText(this)
        var number = edittext.text.toString()
        alert.setMessage("Podaj numer zestawu LEGO")
        alert.setTitle("Nowy Projekt")
        alert.setView(edittext)
        alert.setPositiveButton("Dodaj", DialogInterface.OnClickListener { dialog, whichButton ->
            num = edittext.text.toString()
            showAddNameDialog()
        })
        alert.setNegativeButton("Anuluj", DialogInterface.OnClickListener { dialog, whichButton ->
            num = "0"
        })
        alert.show()
    }

    fun showAddNameDialog(){
        val alert = AlertDialog.Builder(this)
        val edittext = EditText(this)
        var number = edittext.text.toString()
        alert.setMessage("Podaj Nazwe Nowego Projektu")
        alert.setTitle("Nowy Projekt")
        alert.setView(edittext)
        alert.setPositiveButton("Dodaj", DialogInterface.OnClickListener { dialog, whichButton ->
            proj_name = edittext.text.toString()
            if(extension == "xml"){
                download()
            }
            else if(extension == "csv")
            {
                downloadCsv()
            }
        })
        alert.setNegativeButton("Anuluj", DialogInterface.OnClickListener { dialog, whichButton ->
            proj_name = ""
        })
        alert.show()
    }

    fun settings(v : View){
        val intent = Intent(this, Settings::class.java)
        startActivityForResult(intent,EXTENSION_TAG)
    }
}
