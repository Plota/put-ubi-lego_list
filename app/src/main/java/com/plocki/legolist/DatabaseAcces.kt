package com.plocki.legolist

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import java.util.*

class DatabaseAcces(context: Context) {
    private var openHelper: SQLiteOpenHelper = DatabaseHelper(context)
    private lateinit var db: SQLiteDatabase

    fun open(){
        this.db = openHelper.writableDatabase
    }

    fun close(){
        if(db!=null){
            this.db.close()
        }
    }


    fun addColorPL(){
        db.execSQL("Update Colors set NamePL= 'Brozowy' where id = 11")
    }


    fun addProject(name : String, items: ArrayList<Item>): Int {
        var c = db.rawQuery("SELECT * FROM INVENTORIES ORDER BY ID ASC",null)
        var id = 0
        var tmp = false
        while (c.moveToNext()){
            id = c.getInt(0)
            tmp = true
        }
        if(tmp){
            id ++
        }
        val cal = Calendar.getInstance()
        val time = cal.time.time
        db.execSQL("INSERT INTO INVENTORIES(id,Name,LastAccessed) VALUES ($id,'$name',$time);")

        var c1 = db.rawQuery("SELECT * FROM INVENTORIESPARTS ORDER BY ID ASC",null)
        var idParts = 0
        tmp = false
        while (c1.moveToNext()){
            idParts = c1.getInt(0)
            tmp = true
        }
        if(tmp){
            idParts ++
        }

        for(item in items){
            val idtype = item.getTypeID(db)
            val idid = item.getitemidID(db)
            val idcolor = item.getColorID(db)
            val qis = item.qty
            db.execSQL("INSERT INTO INVENTORIESPARTS(ID,INVENTORYID,TYPEID,ITEMID,QuantityInSet,COLORID) VALUES ($idParts,$id,$idtype,$idid,$qis,$idcolor)")
            idParts++
        }
        return id
    }

    fun deleteProjects(){
        db.execSQL("DELETE FROM INVENTORIESPARTS ")
    }

    fun listProjects(): ArrayList<String> {
        var c = db.rawQuery("SELECT * FROM INVENTORIES WHERE ACTIVE = 1",null)
        var list = arrayListOf<String>()
        while (c.moveToNext()){
            val proj_id = c.getInt(0)
            val color = c.getString(1)
            val res = "$proj_id: $color"
            list.add(res)
        }
        return list
    }

    fun getDB(): SQLiteDatabase {
        return db
    }
    fun listParts(invId : Int): ArrayList<String> {
        var c = db.rawQuery("SELECT * FROM INVENTORIESPARTS WHERE INVENTORYID = $invId",null)
        var list = arrayListOf<String>()
        while (c.moveToNext()){
            var id: Int = c.getInt(0)
            var inventoryId : Int = c.getInt(1)
            var typeId : Int = c.getInt(2)
            var itemId : Int = c.getInt(3)
            var quantityInSet : Int = c.getInt(4)
            var quantityInStore : Int = c.getInt(5)
            var colorId : Int = c.getInt(6)
            var extra : Int = c.getInt(7)
            val string ="$id,$inventoryId,$typeId,$itemId,$quantityInSet,$quantityInStore,$colorId,$extra"
            list.add(string)
        }
        return list
    }

    fun updatePart(id : Int, value : Int){
        db.execSQL("UPDATE INVENTORIESPARTS SET QuantityInStore = $value WHERE ID = $id;" );
    }
    fun setNonActive(id : Int){
        db.execSQL("UPDATE INVENTORIES SET ACTIVE = 0 WHERE ID = $id;")
    }
}