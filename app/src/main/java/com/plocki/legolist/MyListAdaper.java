package com.plocki.legolist;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import java.util.ArrayList;

public class MyListAdaper extends ArrayAdapter<String> {

    int layout;
    DatabaseAcces databaseAcces;

    ArrayList<InventoryItem> list = new ArrayList<>();

    MyListAdaper(Context context, int resource, ArrayList<String> objects, DatabaseAcces databaseAcces) {
        super(context, resource,objects);
        this.databaseAcces = databaseAcces;
        for(String object : objects){
            InventoryItem tmp = new InventoryItem(object);
            list.add(tmp);
        }
        layout = resource;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = new ViewHolder();

        if(convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(layout, parent, false);
            viewHolder.color = (TextView) convertView.findViewById(R.id.color);
            viewHolder.name = (TextView) convertView.findViewById(R.id.name);
            viewHolder.max = (TextView) convertView.findViewById(R.id.max);
            viewHolder.act = (TextView) convertView.findViewById(R.id.act);
            viewHolder.button_minus = (Button) convertView.findViewById(R.id.list_item_btn_min);
            viewHolder.button_plus = (Button) convertView.findViewById(R.id.list_item_btn_plus);
            convertView.setTag(viewHolder);
        }
        else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.button_minus.setTag(viewHolder);
        viewHolder.button_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewHolder tmpviev = (ViewHolder)v.getTag();
                int tmp = list.get(position).getQuantityInSet();
                if(list.get(position).getQuantityInSet() > 0){
                    tmp = tmp - 1;
                    tmpviev.act.setText(String.valueOf(tmp));
                    InventoryItem inv = list.get(position);
                    inv.setQuantityInSet(tmp);
                    list.set(position,inv);
                    databaseAcces.updatePart(inv.getId(),tmp);
                }
                else{
                    Toast.makeText(getContext(), "Nie wolno", Toast.LENGTH_SHORT).show();
                }
            }
        });

        viewHolder.button_plus.setTag(viewHolder);
        viewHolder.button_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewHolder tmpviev = (ViewHolder)v.getTag();
                int tmp = list.get(position).getQuantityInSet();
                if(list.get(position).getQuantityInSet() != list.get(position).getQuantityInStore()){
                    tmp = tmp + 1;
                    tmpviev.act.setText(String.valueOf(tmp));
                    InventoryItem inv = list.get(position);
                    inv.setQuantityInSet(tmp);
                    list.set(position,inv);
                    databaseAcces.updatePart(inv.getId(),tmp);
                }
                else{
                    Toast.makeText(getContext(), "Nie wolno", Toast.LENGTH_SHORT).show();
                }
            }
        });
//
        viewHolder.color.setText(list.get(position).getColorName(databaseAcces.getDB()));
        viewHolder.name.setText(list.get(position).getItemName(databaseAcces.getDB()));
        viewHolder.max.setText(String.valueOf(list.get(position).getQuantityInStore()));
        viewHolder.act.setText(String.valueOf(list.get(position).getQuantityInSet()));
        return convertView;
    }
    class ViewHolder {
        TextView name = null;
        TextView color = null;
        TextView act = null;
        TextView max = null;
        Button button_minus = null;
        Button button_plus = null;
    }

}