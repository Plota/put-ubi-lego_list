package com.plocki.legolist

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper

class DatabaseHelper : SQLiteAssetHelper{

    constructor(context: Context) : super(context, "BrickList.db",null,1)

}