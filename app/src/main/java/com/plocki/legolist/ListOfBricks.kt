package com.plocki.legolist

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import kotlinx.android.synthetic.main.activity_list_of_bricks.*


class ListOfBricks : AppCompatActivity() {

    var invetoryId = 0
    lateinit var  databaseAcces: DatabaseAcces
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_of_bricks)
        val intent: Intent = getIntent()
        invetoryId = intent.getStringExtra("inventoryId").toInt()

        databaseAcces = DatabaseAcces(applicationContext)
        databaseAcces.open()


        var list = databaseAcces.listParts(invetoryId)

        val arrayAdapter = MyListAdaper(this,com.plocki.legolist.R.layout.list_of_item,list,databaseAcces)
        partList.adapter = arrayAdapter
        print("dupa")
    }
    public fun close(v: View){
        finish()
    }


    public fun delete(v: View){
        databaseAcces.setNonActive(invetoryId)
        finish()
    }

    public fun export(v: View){
        val intent = Intent(applicationContext, Export::class.java)
        intent.putExtra("inventoryId", invetoryId.toString())
        startActivity(intent)
    }
}
